# Curtain

So, I've been running [pi-hole](http://pi-hole.net/) for a while, and while it's an awesome project, there is just something about that use of PHP that makes me shudder, hence Curtain was born.  The idea of this project isn't to replace pi-hole, it's more of a, "can it be done in Python" project for me.  If others find it useful, then that's a bonus.

## Installing

### DynamoDB

I'm currently "testing" ( as in, I haven't used it in anger before, so I'm seeing what it's like here, I'll probably add other DB's once I've got it all working...  I quite like the idea of using FoundationDB, too ) DynamoDB.  The easiest way to get DynamoDB - at least on a Mac - is using Brew ( I'm assuming that Java is already installed ).

```
brew install dynamodb-local
```

And then run it with:

```
dynamodb-local --dbPath ~/Workspace/Data/DynamoDB -sharedDb
```

### Curtain

