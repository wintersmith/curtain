import sqlite3

dbSchema = """CREATE TABLE Queries ( id INTEGER PRIMARY KEY AUTOINCREMENT, 
timestamp INTEGER NOT NULL, 
type INTEGER NOT NULL, 
status INTEGER NOT NULL, 
domain TEXT NOT NULL, 
client TEXT NOT NULL, 
forward TEXT );
CREATE TABLE Hosts ( id INTEGER PRIMARY KEY AUTOINCREMENT,
ip text not null,
domain text not null,
block integer not null );
CREATE INDEX idxQueriesTimestamps ON queries ( timestamp );
CREATE INDEX idxHostsDomain ON Hosts( domain );
CREATE TABLE network ( id INTEGER PRIMARY KEY NOT NULL, ip TEXT NOT NULL, hwaddr TEXT NOT NULL, interface TEXT NOT NULL, name TEXT, firstSeen INTEGER NOT NULL, lastQuery INTEGER NOT NULL, numQueries INTEGER NOT NULL,macVendor TEXT);

"""

def dict_factory( cursor, row ):
    return dict( ( cursor.description[ idx ][ 0 ], value) for idx, value in enumerate( row ) )


class DatabaseDrive( object ):

    def __init__( self, dbConfig ):
        self._dbConfig = dbConfig
        self._dbConn = None

    def connect( self ):
        try:
            self._dbConn = sqlite3.connect( self._dbConfig[ 'Schema' ] )
            self._dbConn.row_factory = dict_factory
        except sqlite3.OperationalError as errMsg:
            print( "Failed Connecting To DB: %s" % errMsg )

    def _checkExists( self ):
        if not self._dbConn is None:
            doesExist = "SELECT name FROM sqlite_master WHERE type='table' AND name='Dict'"
            with self._dbConn:
                tableFound = self._dbConn.execute( doesExist ).fetchone()
                if not tableFound:
                    for indivCmd in dictSchema.split( ';' ):
                        try:
                            self._dbConn.execute( '{};'.format( indivCmd ) )
                        except sqlcipher.DatabaseError as errMsg:
                            print( "Couldn't Execute Command {} - {}".format( indivCmd, errMsg ) )
