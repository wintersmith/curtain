from json import dumps

import boto3

from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr



domainTable = {
    'TableName': 'Domain',
    'KeySchema': [
        {
            'AttributeName': 'domain',
            'KeyType': 'HASH'
        }
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'domain',
            'AttributeType': 'S'
        }
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
}
queryTable = {
    'TableName': 'Query',
    'KeySchema': [
        {
            'AttributeName': 'domain',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'timeStamp',
            'KeyType': 'S'
        }    
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'domain',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'timeStamp',
            'AttributeType': 'S'
        }    
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
}
cacheTable = {
    'TableName': 'Cache',
    'KeySchema': [
        {
            'AttributeName': 'ipAddress',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'domain',
            'KeyType': 'RANGE'
        },
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'ipAddress',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'domain',
            'AttributeType': 'S'
        }
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }

}
logTable = {
    'TableName': 'Logging',
    'KeySchema': [
        {
            'AttributeName': 'LoggerName',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'timeStamp',
            'KeyType': 'RANGE'
        },
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'LoggerName',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'timeStamp',
            'AttributeType': 'S'
        }
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }

}
clientTable = {
    'TableName': 'Client',
    'KeySchema': [
        {
            'AttributeName': 'ipAddress',
            'KeyType': 'HASH'
        },
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'ipAddress',
            'AttributeType': 'S'
        },
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }

}

class DatabaseDriver( object ):

    def __init__( self, dbConfig ):
        self._dbConfig = dbConfig
        self._dbRes = None
        self._dbClient = None

    def connect( self ):
        try:
            self._dbRes = boto3.resource( 'dynamodb', **self._dbConfig )
            self._dbClient = boto3.client( 'dynamodb', **self._dbConfig )
        except Exception as errMsg:
            print( errMsg )
        self._createTables()

    def _createTables( self ):
        ddbResp = None
        for tableSchema in [ domainTable, queryTable, cacheTable, logTable ]:
            try:
                ddbResp = self._dbClient.create_table( **tableSchema )
            except ClientError as errMsg:
                if errMsg.response[ 'Error'][ 'Code' ] == 'ResourceInUseException':
                    print( 'Ignoring ', errMsg )
            # finally:
            #     self._dbClient.get_waiter( 'table_exists' ).wait( TableName = tableSchema[ 'TableName' ] )

    def lookUp( self, **kwArgs ):
        ddbResp = {}
        ddbTable = self._dbRes.Table( kwArgs[ 'tableName' ] )
        ddbResp = ddbTable.scan( FilterExpression = Attr( kwArgs[ 'keyName' ] ).eq( kwArgs[ 'lookUp' ] ) )
        if any( ddbResp[ 'Items' ] ):
            for ddbRow in ddbResp[ 'Items' ]:
                print( ddbRow )
                yield ddbRow

        return None

    def insert( self, dbDict ):
        ddbTable = self._dbRes.Table( dbDict[ 'tableName' ] )
        ddbResp = ddbTable.put_item( Item = dbDict[ 'newItem' ] )

        return ddbResp

    def dump( self, tableName ):
        ddbPaginator = self._dbClient.get_paginator( 'scan' )
        for iPage in ddbPaginator.paginate( TableName = tableName ):
            yield from iPage[ 'Items' ]
