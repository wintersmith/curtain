import importlib


# dbDriver = importlib.import_module( '._%s' % webApp.config[ 'DB_TYPE' ], 'Core' )

dbDriver = importlib.import_module( '._%s' % 'DDB'.lower(), 'Core' )
dbClass = getattr( dbDriver,'DatabaseDriver' )

class Database( object ):

    def __init__( self, dbConfig ):
        self._dbConfig = dbConfig
        # hostConfig = []
        # if type( dbHost ) is list:
        #     for listIndex, indivHost in enumerate( dbHost ):
        #         hostConfig.append( ( indivHost, dbPort[ listIndex] if type( dbPort ) is list else dbPort ) )
        # else:
        #     hostConfig.append( ( dbHost, dbPort[ 0 ] if type( dbPort ) is list else dbPort ) )

        # self._dbConfig[ 'Hosts' ] = hostConfig
        # self._dbConfig[ 'User' ] = dbUser
        # self._dbConfig[ 'Passwd' ] = dbPass
        # self._dbConfig[ 'Schema' ] = dbSchema

        self._dbDriver = dbClass( self._dbConfig )

    def connect( self ):
        self._dbDriver.connect()

    def lookUp( self, **kwArgs ):
        return self._dbDriver.lookUp( **kwArgs )

    def insert( self, dbDict ):
        return self._dbDriver.insert( dbDict )

    def dump( self, tableName ):
        return self._dbDriver.dump( tableName )