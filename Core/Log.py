import logging
import json

from datetime import datetime
from time import time
from decimal import Decimal

from . import Database

dbConn = Database.Database( { 'endpoint_url': 'http://localhost:8000' } )
dbConn.connect()

class DynamoDBFormatter( logging.Formatter ):

    def format(self, record):
        data = record.__dict__.copy()

        if record.args:
            msg = record.msg % record.args
        else:
            msg = record.msg
        data.update(
            args=str([unicode(arg) for arg in record.args]),
            message=str(msg),
        )

        # # Remove empty keys from the dictionary
        # data = dict((k, v) for k, v in data.iteritems() if v)

        if 'exc_info' in data and data['exc_info']:
            data['exc_info'] = self.formatException(data['exc_info'])

        return json.loads( json.dumps( data ), parse_float = Decimal )


class DynamoDBHandler( logging.Handler ):

    def __init__(self, level = logging.NOTSET ):
        logging.Handler.__init__( self, level )

        self.formatter = DynamoDBFormatter()

    def emit(self, record):
        # try:

        formatted_record = self.format( record )

        logPayLoad = { 'tableName': 'Logging', 'newItem': { 'LoggerName': formatted_record['name'], 
                                                            'timeStamp': str( int( time() ) ), 
                                                            'logMessage': formatted_record } }
        dbConn.insert( logPayLoad )

        # except Exception as errMsg:
        #     logging.error( "Unable to save log record: {}".format( errMsg ) )
            
def addLogEntry( clientAddress, clientPort, dnsQuery, dnsQueryType ):
    logPayLoad = { 'tableName': 'Query', 'newItem': { 'domain': dnsQuery, 'timeStamp': str( int( time() ) ), 'clientAddress': clientAddress, 'clientPort': clientPort } }
    dbConn.insert( logPayLoad )