
from . import Database

dbConn = Database.Database( { 'endpoint_url': 'http://localhost:8000' } )
dbConn.connect()


def queryCache( dnsName, dnsType ):
    dbResp = dbConn.lookUp( tableName = 'Domain', lookUp = dnsName, keyName = 'domain' )
    if not any( dbResp ):
        dbResp = dbConn.lookUp( tableName = 'Cache', lookUp = dnsName, keyName = 'domain' )
    else:
        print( 'Blocked' )
        yield { 'ipAddress': '127.0.0.1', 'dnsType': 1, 'ttl': '300', 'domain': dnsName }

    return dbResp

def addToCache( dnsName, dnsType, dnsAnswer, dnsTtl ):
    dbPayLoad = { 'tableName': 'Cache', 'newItem': { 'domain': dnsName, 'ipAddress': dnsAnswer, 'dnsType': dnsType, 'ttl': dnsTtl } }
    dbConn.insert( dbPayLoad )

def dumpCache( ):
    for iRow in dbConn.dump( 'Cache' ):
        print( iRow )