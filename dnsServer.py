import sys
import os
import logging

from datetime import datetime
from time import sleep, time

import dnslib

from requests import get as httpGet
from dnslib.server import DNSServer, BaseResolver, DNSLogger, QTYPE
from dnslib.label import DNSLabel
from dnslib import RR, A, MX, CNAME, SOA

from Core import Cache, Log


cloudFlareDNS = { 'url': 'https://cloudflare-dns.com/dns-query', 'Params': { 'ct': 'application/dns-json' } }
googleDNS = { 'url': 'https://dns.google.com/resolve' }
cacheUrl = 'https://127.0.0.1/api/v1/query'

logFacility = logging.getLogger( 'dnsServer' )
logFacility.setLevel( logging.DEBUG )
dbHandler = Log.DynamoDBHandler()
logFacility.addHandler( dbHandler )


class DnsCache( object ):

    def __init__( self ):
        self._seenDomain = {}

    def __setitem__( self, key, item ):
        self._seenDomain[ key ] = { 'ip': item, 'timestemp': datetime.now() }

    def __contains__( self, key ):
        return key in self._seenDomain


class DoHResolver( BaseResolver ):

    def __init__( self, dohProvider ):
        logFacility.debug( 'Setting DoH Provider To {}'.format( dohProvider ) )
        self._dohDict = dohProvider

    def resolve( self, request, handler ) -> RR:
        dnsReply = request.reply()
        dnsQueryType = QTYPE[ request.q.qtype ]
        dnsQueryHost = str( request.q.qname )
        clientIp = handler.client_address[ 0 ]
        foundInCache = False
        for indivAnswer in Cache.queryCache( dnsQueryHost, dnsQueryType ):
            foundInCache = True
            logFacility.debug( 'Received Response From Cache {}'.format( indivAnswer ) )
            rrType = getattr( dnslib, QTYPE[ indivAnswer[ 'dnsType' ] ] )( indivAnswer[ 'ipAddress' ] )
            dnsReply.add_answer( RR( indivAnswer[ 'domain' ], rdata = rrType, ttl = int( indivAnswer[ 'ttl' ] ), rtype = int( indivAnswer[ 'dnsType' ] ) ) )
        if not foundInCache:
            print( "Not Found In Cache" )
            logFacility.debug( 'Nothing Found In Cache, Querying DoH Provider' )
            dohResponse = self._queryDoH( dnsQueryHost, dnsQueryType )
            print( dohResponse )
            for keyWord in [ 'Answer', 'Authority' ]:
                if keyWord in dohResponse:
                    for indivAnswer in dohResponse[ keyWord ]:
                        rrType = None
                        if dnsQueryType == 'MX':
                            mxPref, mxData = indivAnswer[ 'data' ].split( ' ' )
                            rrType = MX( mxData, preference = int( mxPref ) )
                        elif dnsQueryType == 'CNAME':
                            dnsOrigin, dnsMailAddr, dnsSerial, dnsRefresh, dnsRetry, dnsExpire, dnsMin = indivAnswer[ 'data' ].split( ' ' )
                            rrType = CNAME( dnsOrigin, dnsMailAddr, ( int( dnsSerial ), int( dnsRefresh ), int( dnsRetry ), int( dnsExpire ), int( dnsMin ) ) )
                        # elif dnsQueryType == 'A':
                        else:
                            rrType = getattr( dnslib, QTYPE[ indivAnswer[ 'type' ] ] )( indivAnswer[ 'data' ] )
                        dnsReply.add_answer( RR( indivAnswer[ 'name' ], rdata = rrType, ttl = indivAnswer[ 'TTL' ], rtype = indivAnswer[ 'type' ] ) )
                        Cache.addToCache( dnsQueryHost, indivAnswer[ 'type' ], indivAnswer[ 'data' ], indivAnswer[ 'TTL' ] )

                    break
                
        return dnsReply

    def _queryDoH( self, queryHost: str, queryType: str ) -> dict:
        baseParams = { 'name': queryHost, 'type': queryType }
        dnsParams = baseParams.copy()
        dohAnswer = {}
        if 'Params' in self._dohDict:
            dnsParams.update( self._dohDict[ 'Params' ] )
            for _ in range( 5 ):
                try:
                    dohResp = httpGet( self._dohDict[ 'url' ], params = dnsParams )
                    if dohResp.status_code == 200:
                        logFacility.debug( 'Response From Provider - {}'.format( dohResp.json() ) )
                        dohAnswer = dohResp.json()
                        break
                except Exceptions as errMsg:
                    logFacility.error( 'Encountered Error {} With Query {} To {}'.format( errMsg, self._dohDict[ 'url' ], dnsParams ) )
                    sleep( 5 )

        return dohAnswer


class DoHLogger( DNSLogger ):

    def log_request( self, handler, request ):
        clientAddress = handler.client_address[ 0 ]
        clientPort = handler.client_address[ 1 ]
        Log.addLogEntry( clientAddress, clientPort, str( request.q.qname ), QTYPE[ request.q.qtype ] )


def main():

    dohResolver = DoHResolver( cloudFlareDNS )
    dnsLogger = DoHLogger( prefix=False )
    dnsServer = DNSServer( dohResolver, port = 53, address = "0.0.0.0", logger = dnsLogger )

    retCode = 0
    try:
        dnsServer.start()
    except KeyboardInterrupt:
        print( "Shutting Down...." )
    except Exception as errMsg:
        print( "Encountered An Error Running DNS Server." )
        print( errMsg )
        retCode = 1

    return retCode


if __name__ == '__main__':
    sys.exit( main() )