import os
import sys

from dnslib.client import DNSRecord


def main():
    dnsQuery = DNSRecord.question( sys.argv[ 1 ] )
    dnsAnswer = dnsQuery.send( "localhost", 8053 )
    print( DNSRecord.parse( dnsAnswer ) )

if __name__ == '__main__':
    sys.exit( main() )